var site = site || {};
  site.window = $(window);
  site.document = $(document);
  site.Width = site.window.width();
  site.Height = site.window.height();
  var indices_verts = [];
  var Background = function() {
  
  };
  
  Background.headparticle = function() {   
  
     if ( !Modernizr.webgl ) {
        alert('Your browser dosent support WebGL');
     }
  
     var camera, scene, renderer;
     var mouseX = 0, mouseY = 0;
     var p;
  
     var windowHalfX = site.Width / 2;
     var windowHalfY = site.Height / 2;
  
     Background.camera = new THREE.PerspectiveCamera( 8, site.Width / site.Height, 1, 1000 );
     Background.camera.position.z = 700;
     
  
     // scene
     Background.scene = new THREE.Scene();
  
     // texture
     var manager = new THREE.LoadingManager();
     manager.onProgress = function ( item, loaded, total ) {
        //console.log('webgl, twice??');
        //console.log( item, loaded, total );
     };
  
  
     // particles
	 var p_geom = new THREE.Geometry();
	 
     var p_material = new THREE.ParticleBasicMaterial({
        color: 0x02FEFF,
        size: 1.5
	 });
  
	 var color = new THREE.Color();
	 color.setRGB(255, 255, 255);
	 var textMaterial = new THREE.MeshBasicMaterial({color: color});
	//  var font;
	// 	var loaderFont = new THREE.FontLoader();
	// 	loaderFont.load( 'http://localhost:8000/static/font.json', function ( response ) {
	// 		font = response;
	// 	});

     // model
     var loader = new THREE.OBJLoader( manager );
     loader.load( 'http://localhost:8000/static/model_head.obj', function ( object ) {
        object.traverse( function ( child ) {
  
           if ( child instanceof THREE.Mesh ) {
  
              // child.material.map = texture;
  
              var scale = 6;
  
              $(child.geometry.vertices).each(function(i) {
				 p_geom.vertices.push(new THREE.Vector3(this.x * scale - 15, this.y * scale, this.z * scale));
				
				 indices_verts.push({'x': this.x * scale, 'y': this.y * scale - 100, 'z': this.z * scale, 'id': i});

				// textgeom = new THREE.TextGeometry(""+i, {size: 5, font: font});
				// meshGeom = new THREE.Mesh(textgeom, textMaterial);
				// meshGeom.position.x = this.x * scale;
				// meshGeom.position.y = this.y * scale;
				// meshGeom.position.z = this.z * scale;

				// Background.scene.add(meshGeom);
              })
           }
        });
  
        Background.scene.add(p)
     });
  
     p = new THREE.ParticleSystem(
        p_geom,
        p_material
	 );
  
     Background.renderer = new THREE.WebGLRenderer({ alpha: true });
     Background.renderer.setSize( site.Width, site.Height );
     Background.renderer.setClearColor(0x000000, 0);
  
     $('.particlehead').append(Background.renderer.domElement);
	//  $(document).on('mousemove', onDocumentMouseMove);
	clicked = false;


	$(document).click(function(){
		if (clicked){
			newmaterial = new THREE.ParticleBasicMaterial({ color: 0xFFFFFF, size: 1.5});
			p.material = newmaterial;
		}else{
			p_material = new THREE.ParticleBasicMaterial({
				color: 0x02FEFF,
				size: 1.5
			});
			p.material = p_material;
		}
		clicked = clicked == true ? false : true;
	});
     site.window.on('resize', onWindowResize);
  
     function onWindowResize() {
        windowHalfX = site.Width / 2;
        windowHalfY = site.Height / 2;
        //console.log(windowHalfX);
  
        Background.camera.aspect = site.Width / site.Height;
        Background.camera.updateProjectionMatrix();
  
        Background.renderer.setSize( site.Width, site.Height );
     }
  
     function onDocumentMouseMove( event ) {
        xPos = ( event.clientX - windowHalfX ) / 2;
		yPos = ( event.clientY - windowHalfY ) / 2;
	 }
	 
	 var intervaloMovimiento;
	 Background.onSpeaking = function(){
			posx = Math.floor((Math.random() * 600) - 300);
			posy = Math.floor((Math.random() * 510) - 255);

			mouseX = posx;
			mouseY = posy;

		console.log(mouseX, mouseY);
	 }

	 Background.stopSpeaking = function(){
		mouseX = 0;
		mouseY = 0;
	 }
  
     Background.animate = function() { 
  
        Background.ticker = TweenMax.ticker;
        Background.ticker.addEventListener("tick", Background.animate);
  
        render();
     }
  
     function render() {
        Background.camera.position.x += ( (mouseX * .5) - Background.camera.position.x ) * .05;
        Background.camera.position.y += ( -(mouseY * .5) - Background.camera.position.y ) * .05;
  
        Background.camera.lookAt( Background.scene.position );
  
        Background.renderer.render( Background.scene, Background.camera );
     }
  
     render();
  
     Background.animate();
  };
  
  
  Background.headparticle();






const DEFAUL_TEXT='Escribe tu respuesta aqui';
const DEFAULT_TEXT_RANDOM='Una opcion de muchas';

//var socket = io.connect('http://localhost:8080');

if ('speechSynthesis' in window) {
	
}

$('.core2').css('animation', "flicker2 0s infinite");

function tab(n){
	var r=''
	for(var x=0;x<n;x++){
		r+='\t';
	}
	return r;
}

var recognition;
var recognizing = false;

var session_id = "";


// recognition = new webkitSpeechRecognition();
// recognition.lang = "es-CO";
// recognition.continuous = true;
// recognition.interimResults = false;


var palabrasRepeticion = ["COMO", "PODRIAS REPETIR", "ME PUEDES REPETIR", "REPITE POR FAVOR", "REPITE","PODRIAS REPETIRME","POR FAVOR PODRIAS REPETIRME","REPITEME"];
var palabrasRepeticionRespuesta = ["CLARO QUE SI.", "POR SUPUESTO.", "ESTABA DICIENDO."];

var normalize = (function() {
  var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÇç", 
      to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuucc",
      mapping = {};
 
  for(var i = 0, j = from.length; i < j; i++ )
      mapping[ from.charAt( i ) ] = to.charAt( i );
 
  return function( str ) {
      var ret = [];
      for( var i = 0, j = str.length; i < j; i++ ) {
          var c = str.charAt( i );
          if( mapping.hasOwnProperty( str.charAt( i ) ) )
              ret.push( mapping[ c ] );
          else
              ret.push( c );
      }      
      return ret.join( '' );
  }
 
})();

// function hablar(msn,callback){
// 	var msg_g = new SpeechSynthesisUtterance(msn);
// 	msg_g.rate = 0.1; // 0.1 to 10
// 	msg_g.pitch = 1; //0 to 2
// 	window.speechSynthesis.speak(msg_g);
// 	msg_g.onstart = function(event){
// 		console.log("Está hablando");
// 		recognition.stop();
// 	};
		    	
// 	msg_g.onend = function(event){
//         document.getElementsByClassName('core2').style.animation = "flicker2 0s infinite";
//         //$('.core2').css('animation', "flicker2 0s infinite");
// 		console.log("Ha dejado de hablar");
// 		recognition.start();	
// 		if(callback)
// 			callback();
// 	};
// }
var chat_div=$(".chat")

function bajarScroll(){
	
	chat_div.scrollTop(chat_div.prop("scrollHeight") + 200);
}

var fecha = new Date();
hora = fecha.getHours();
minutos = fecha.getMinutes();
dia = fecha.getDate();
mes = fecha.getMonth();

function s4() {
	return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}

meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
diasSemama = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"]
document.getElementById('diaActual').innerHTML = dia;
document.getElementById('mesActual').innerHTML = meses[mes];
document.getElementById('horaActual').innerHTML = (""+hora.length == 1 ? "0"+hora : hora) + ":" + (""+minutos.length == 1 ? "0" + minutos : minutos);
document.getElementById('diaSemana').innerHTML = diasSemama[fecha.getDay() - 1];
setInterval(function(){
	Background.onSpeaking()
}, 5000);


let audio1 = document.getElementById("audio");
let audio2 = document.getElementById("audio2");
let audio3 = document.getElementById("audio3");
let audio4 = document.getElementById("audio4");

document.addEventListener("keypress", function(k){
	switch(k.key){
		case "1":
			audio1.play();
			break;
		case "2":
			audio2.play();
			break;
		case "3":
			audio3.play();
			break;
		case "4":
			audio4.play();
			break;
	}
});

angular.module('aiml',[])
	.directive('bootstrapTab',function(){
		return {
			restrict:'EA',
			compile:function(elm){
				console.log('tabs')
				$('#tabs a').on('click', function (e) {
				  e.preventDefault()
				  $(this).tab('show')
				})
			}
		}
	})
	.controller('main',function($scope,$http,$timeout){

		$scope.nuevoMensaje=''
		$scope.mensajes=[]

	    //socket.on('connect', function(data) {
			// socket.emit('msn', {msn:"REQUEST_SESSION"});
			// socket.emit('enter room', {'room': "ROOM_"+s4()});
		//});
		
		// socket.on('disconnect', function(data){
		// 	console.log(data);
		// });

	    // socket.on('msn', function(data) {
		// 	// if(data.indexOf('SESSION_CODE') > -1){
		// 	// 	session_id = data.replace('SESSION_CODE', '');
		// 	// }else{
		// 	$timeout(function() {
		// 		var msg = new SpeechSynthesisUtterance(data);
		// 		msg.rate = 10; // 0.1 to 10
		// 		msg.pitch = 0; //0 to 2
		// 		window.speechSynthesis.speak(msg);
		// 		$scope.mensajes.push({human:false,msn:data,rep:true});
		// 		refreshNotes();
		// 		bajarScroll();
		// 		msg.onstart = function(event){
		// 			document.getElementsByClassName('core2')[0].style.animation = "flicker2 0.2s infinite";
		// 			console.log("Está hablando");
		// 			recognition.stop();	
		// 			$scope.recognizing = false;
		// 		};
				
		// 		msg.onend = function(event){
		// 			document.getElementsByClassName('core2')[0].style.animation = "flicker2 0s infinite";
		// 			console.log("Ha dejado de hablar");
		// 			recognition.start();
		// 			$scope.recognizing = true;
		// 		};
		// 	},1)
		// 	// }
	    // });
	    

	    // recognition.onstart = function() {
		// 	recognizing = true;
		// 	console.log("empezando a eschucar");
		// 	$('.listenBtn').css("animation", "flicker2 0.2s infinite");
		// }
		// recognition.onresult = function(event) {
		// 	console.log(event.results)
		// 	 for (var i = event.resultIndex; i < event.results.length; i++) {
		// 		if(event.results[i].isFinal){
		// 			var mensaje=event.results[i][0].transcript
		// 			console.log(mensaje)
		// 			mensajeRep = normalize(mensaje.toUpperCase());

		// 			$scope.mensajes.push({human:true,msn:mensajeRep,rep:false})
		// 			refreshNotes();

		// 			if(palabrasRepeticion.indexOf(mensajeRep) > -1){
		// 				$scope.mensajes.push({human:true,msn:mensajeRep,rep:false})
		// 				var encontro=false;
		// 				for (var i = $scope.mensajes.length - 1; i >= 0; i--) {
		// 					if($scope.mensajes[i].rep && $scope.mensajes[i].msn!=''){
		// 						encontro=true;
		// 						console.log('mensaje a repetir',$scope.mensajes[i].msn)
		// 						msgToEmit = palabrasRepeticionRespuesta[Math.floor(Math.random()*palabrasRepeticionRespuesta.length)];
		// 						$scope.mensajes.push({human:false,msn:msgToEmit,rep:false});

		// 						// hablar(msgToEmit,function(){
		// 						// 	$timeout(function(){
		// 						// 		$scope.mensajes.push({human:false,msn:$scope.mensajes[i].msn,rep:true});
		// 						// 		hablar($scope.mensajes[i].msn,null)
		// 						// 	},80)
		// 						// })
								
		// 						return;
		// 					}
		// 				}
		// 				if(!encontro){
		// 					console.log('No encontro palabra para repetir')
		// 				}
						
						
		// 			}else{
		// 				// mensajeRep += '-'+session_id;
		// 				//socket.emit('msn', {msn:mensajeRep});
		// 				console.log("Nada")
		// 			}					
		// 	    }
		// 	}
		// }
		// recognition.onerror = function(event) {
			
		// 	console.log(event)
		// }
		// recognition.onend = function() {
		// 	console.log('Termino d escuchar')
		// 	recognizing = false;
		// 	$('.listenBtn').css("animation", "flicker2 0s infinite");
		// }

		function refreshNotes(){
			var htmlInNotes = "";
			for (var i = 0; i < $scope.mensajes.length; i++) {
				if($scope.mensajes[i].human){
					htmlInNotes += "▶ " + $scope.mensajes[i].msn + "\n";
				}else{
					htmlInNotes += "▶▶ " + $scope.mensajes[i].msn + "\n";
				}
			}
			$('#note_input').html(htmlInNotes);
		}

		$scope.output=''
		$scope.categorias=[]
		$scope.nuevaVariable=''
		$scope.variables=[];

		$scope.recognizing = false;
		$scope.escuchar = function(){
			if($scope.recognizing){
				// recognition.stop();	
				$scope.recognizing = false;

			}else{
				// recognition.start();
				$scope.recognizing = true;
			}
		}

		$scope.enviarComentario = function(){
			let mensaje = $('#textMsg').val();
			mensajeRep = normalize(mensaje.toUpperCase());

			$scope.mensajes.push({human:true,msn:mensajeRep,rep:false})
			refreshNotes();

			if(palabrasRepeticion.indexOf(mensajeRep) > -1){
				$scope.mensajes.push({human:true,msn:mensajeRep,rep:false})
				var encontro=false;
				for (var i = $scope.mensajes.length - 1; i >= 0; i--) {
					if($scope.mensajes[i].rep && $scope.mensajes[i].msn!=''){
						encontro=true;
						console.log('mensaje a repetir',$scope.mensajes[i].msn)
						msgToEmit = palabrasRepeticionRespuesta[Math.floor(Math.random()*palabrasRepeticionRespuesta.length)];
						$scope.mensajes.push({human:false,msn:msgToEmit,rep:false});

						// hablar(msgToEmit,function(){
						// 	$timeout(function(){
						// 		$scope.mensajes.push({human:false,msn:$scope.mensajes[i].msn,rep:true});
						// 		hablar($scope.mensajes[i].msn,null)
						// 	},80)
						// })
						
						return;
					}
				}
				if(!encontro){
					console.log('No encontro palabra para repetir')
				}
				
				
			}else{
				// mensajeRep += '-'+session_id;
				//socket.emit('msn', {msn:mensajeRep});
				console.log("Nada")
			}		
		}
	})